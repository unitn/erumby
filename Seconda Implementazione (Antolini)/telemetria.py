from Tkinter import *
import paramiko
import sys
import os
import time


root= Tk()
inizio_connessione=IntVar()
stop_connessione=0
inizio=0
titolo1=''
w_target1=''

os.environ["PYTHONUNBUFFERED"] = "1" #per eliminare il buffer
cmd = 'cd /mnt/sda1/arduino/www ;python pwm_g2.py'



ant_sin= StringVar()
ant_des= StringVar()
pos_sin= StringVar()
pos_des= StringVar()
w_media= StringVar()
w_target=StringVar()

acc_x=StringVar()
acc_y=StringVar()
acc_z=StringVar()


yaw=StringVar()
pitch=StringVar()
roll=StringVar()

yaw_rate=StringVar()
pitch_rate=StringVar()
roll_rate=StringVar()




duty_motore=StringVar()
duty_sterzo=StringVar()

micro_motore=StringVar()
micro_sterzo=StringVar()


mod=StringVar()
tempo_yun=StringVar()

titolo=StringVar()
var= StringVar()
var.set('ciao')

intero=IntVar()
intero.set(5)


x=0






        
#### generazione finestra


root.title("Telemetria eRUMBY")
root.resizable(0,0)
w=Canvas(root,width=1000,height=650)
w.pack()
w.configure(background='green')
w.create_text(500,50,font=6,text='Telemetria eRUMBY')  

y_w=300
y_acc=400
y_eul=500
y_tempo=600
y_eul_rate=600

### generazione vari label e bottoni:

################ VELOCITA ANGOLARI  ######################
w.create_text(115,y_w-10,text=' anteriore sx')    
a_sin=Label(root, textvariable=ant_sin)
a_sin.pack()
a_sin.place(x=80,y=y_w,width=80,height=20)

w.create_text(235,y_w-10,text=' anteriore dx')    
a_des=Label(root, textvariable=ant_des)
a_des.pack()
a_des.place(x=200,y=y_w,width=80,height=20)

w.create_text(355,y_w-10,text=' posteriore sx')    
p_sin=Label(root, textvariable=pos_sin)
p_sin.pack()
p_sin.place(x=320,y=y_w,width=80,height=20)

w.create_text(475,y_w-10,text=' posteriore dx')    
p_des=Label(root, textvariable=pos_des)
p_des.pack()
p_des.place(x=440,y=y_w,width=80,height=20)


w.create_text(635,y_w-10,text=' w media')    
w_med=Label(root, textvariable=w_media)
w_med.pack()
w_med.place(x=600,y=y_w,width=80,height=20)

w.create_text(115,y_w-60,text=' w  target :')    
w_tar=Label(root, textvariable=w_target)
w_tar.pack()
w_tar.place(x=200,y=y_w-60-10,width=80,height=20)


##################################################################

##################### ACCELERAZIONI  ##############################

w.create_text(115,y_acc-10,text='acc X')    
acc_X=Label(root, textvariable=acc_x)
acc_X.pack()
acc_X.place(x=80,y=y_acc,width=80,height=20)

w.create_text(235,y_acc-10,text='acc Y')    
acc_Y=Label(root, textvariable=acc_y)
acc_Y.pack()
acc_Y.place(x=200,y=y_acc,width=80,height=20)

w.create_text(355,y_acc-10,text='acc Z')    
acc_Z=Label(root, textvariable=acc_z)
acc_Z.pack()
acc_Z.place(x=320,y=y_acc,width=80,height=20)


##################################################################



##################### ACCELERAZIONI ##############################

w.create_text(115,y_eul-10,text=' YAW')    
l_yaw=Label(root, textvariable=yaw)
l_yaw.pack()
l_yaw.place(x=80,y=y_eul,width=80,height=20)

w.create_text(235,y_eul-10,text='ROLL')    
l_roll=Label(root, textvariable=roll)
l_roll.pack()
l_roll.place(x=200,y=y_eul,width=80,height=20)

w.create_text(355,y_eul-10,text='PITCH')    
l_pitch=Label(root, textvariable=pitch)
l_pitch.pack()
l_pitch.place(x=320,y=y_eul,width=80,height=20)


##################################################################


w.create_text(115,y_eul_rate-10,text=' YAW RATE')    
l_yaw=Label(root, textvariable=yaw_rate)
l_yaw.pack()
l_yaw.place(x=80,y=y_eul_rate,width=80,height=20)

w.create_text(235,y_eul_rate-10,text='ROLL RATE')    
l_roll=Label(root, textvariable=roll_rate)
l_roll.pack()
l_roll.place(x=200,y=y_eul_rate,width=80,height=20)

w.create_text(355,y_eul_rate-10,text='PITCH RATE')    
l_pitch=Label(root, textvariable=pitch_rate)
l_pitch.pack()
l_pitch.place(x=320,y=y_eul_rate,width=80,height=20)










##################### DUTY   ##############################

w.create_text(885,y_acc-10,text='duty motore')    
l_duty_motore=Label(root, textvariable=duty_motore)
l_duty_motore.pack()
l_duty_motore.place(x=850,y=y_acc,width=80,height=20)


w.create_text(885,y_eul-10,text='duty sterzo')    
l_duty_sterzo=Label(root, textvariable=duty_sterzo)
l_duty_sterzo.pack()
l_duty_sterzo.place(x=850,y=y_eul,width=80,height=20)


##################################################################


##################### MILLS RADIOCOMANDO  ##############################

#rettangolo acc
w.create_text(600,y_acc-15,text='posizione acceleratore')

w.create_rectangle(500,y_acc,600 ,y_acc+20 ,fill='white')
w.create_rectangle(600,y_acc,700 ,y_acc+20 ,fill='white')
#rettangolo sterzo
w.create_text(600,y_eul-15,text='posizione sterzo')

w.create_rectangle(500,y_eul,600 ,y_eul+20 ,fill='white')
w.create_rectangle(600,y_eul,700 ,y_eul+20 ,fill='white')
#w.create_rectangle(50,50,600,y_eul,fill='white')


##################################################################


##################### MODALITA  ##############################

w.create_text(885,y_w-10,text='modalita')    
l_mod=Label(root, textvariable=mod)
l_mod.pack()
l_mod.place(x=850,y=y_w,width=80,height=20)


##################################################################

##################### TEMPO  ##############################

w.create_text(885,y_tempo-10,text='tempo')    
tempo=Label(root, textvariable=tempo_yun)
tempo.pack()
tempo.place(x=850,y=y_tempo,width=80,height=20)


##################################################################

################### TITOLO PROVA #############################
w.create_text(115,y_w-60 -30,text=' titolo prova  :')    
w_tar=Label(root, textvariable=titolo)
w_tar.pack()
w_tar.place(x=200,y=y_w-60-10-30,width=100,height=20)

##################################################################
def stop_popup():
    print("popup")
    global x
    x=0
    if int(w_target.get())> 99 or int(w_target.get())<0 or len(w_target.get())==0:
        tkMessageBox.showinfo("ATTENZIONE","valore w target fuori dai limiti (0/99 rad/s)")
        x=1
    if len(titolo.get())==0:
        tkMessageBox.showinfo("ATTENZIONE","Inserire il titolo ")
        x=1
    print(x)
    if x==0:    
        global titolo1
        titolo1=titolo.get()
        global w_target1
        w_target1=w_target.get()
        print (titolo1,w_target1)
        top.destroy()



### funzioni bottoni####
def comando_inizio():
    inizio_connessione.set(1)
    print(inizio_connessione.get())
    print("inizio")
    global inizio
    inizio=1

def comando_fine():
    inizio_connessione.set(0)
    ssh.close()
    global top
        ################  POP UP   ###############
    top=Toplevel()
    top.title("prova")
    top.geometry("200x250+600+200")
    top.attributes("-topmost", True)
    
    

    msg1 = Message( top,width=100,text='inserire w target: ')
    msg1.pack()
    msg1.place(x=50,y=120)

    target=Entry(top,textvariable=w_target)
    target.pack()
    target.place(x=70,y=150,width=50,height=20)

    msg2 = Message( top,width=200,text='inserire nome prova: ')
    msg2.pack()
    msg2.place(x=40,y=50)

    ti=Entry(top,textvariable=titolo)
    ti.pack()
    ti.place(x=30,y=80,width=150,height=20)



    ok = Button(top,text='OK',command=stop_popup)
    ok.pack()
    ok.place(x=80,y=200,width=50,height=30)
###################################################################

    
##################### BOTTONI  ##############################

stop = Button( text='STOP',command=comando_fine)
stop.pack()
stop.place(x=900,y=70,width=80,height=30)

start = Button( text='START',command=comando_inizio)
start.pack()
start.place(x=800,y=70,width=80,height=30)
##################################################################

##################### ENTRY  ##############################

##target=Entry()
##target.pack()
##target.place(x=100,y=100,width=80,height=30)

##################################################################



################  POP UP   ###############
top=Toplevel()
top.title("prova")
top.geometry("200x250+600+200")
top.attributes("-topmost", True)

msg1 = Message( top,width=100,text='inserire w target: ')
msg1.pack()
msg1.place(x=50,y=120)

target=Entry(top,textvariable=w_target)
target.pack()
target.place(x=70,y=150,width=50,height=20)

msg2 = Message( top,width=200,text='inserire nome prova: ')
msg2.pack()
msg2.place(x=40,y=50)

ti=Entry(top,textvariable=titolo)
ti.pack()
ti.place(x=30,y=80,width=150,height=20)



ok = Button(top,text='OK',command=stop_popup)
ok.pack()
ok.place(x=80,y=200,width=50,height=30)
###################################################################






################################# FUNZIONE SSH #############################################################�


        
##ssh = paramiko.SSHClient()
##ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
##conn=ssh.connect('192.168.240.1',port=22, username='root',password='arduino')
##if conn is None:
##         print "Successfully Authenticated"
##
##stdin, stdout, stderr = ssh.exec_command(cmd)
##inzio_connessione =0





    
#stdin.write('5 \n')  ## importante ci vuole il \n se no si blocca perch� non vede l'invio


while True:

    
    if inizio_connessione.get()==1 & inizio==1:
        
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        conn=ssh.connect('192.168.240.1',port=22, username='root',password='arduino')
        if conn is None:
                 print "Connessione avvenuta"

        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.write(titolo1 +'\n')
        stdin.write(w_target1 +'\n')
        global inizio
        inizio=0


     
    if inizio_connessione.get()==1:
        
        #lettura valori provenienti da linino via ssh
        f=stdout.readline()
        print(f)
        eulero=stdout.readline()
        print(eulero)
        acc=stdout.readline()
        velo=stdout.readline()
        micros_radio=stdout.readline()
        duty=stdout.readline()
        modalita=stdout.readline()
        t_yun=stdout.readline()
        rate=stdout.readline()
        f=stdout.readline()
        
        #f=stdout.readline()
        
        #print(modalita)
        #divisione delle righe lette in modo da isolare i singoli valori
        eulero=eulero.split("'")
        acc=acc.split("'")
        velo=velo.split("'")
        duty=duty.split("'")
        micros_radio=micros_radio.split("'")
        t_yun=t_yun.split("\n")
        rate= rate.split("'")
        modalita=modalita.split("\n")
        
        #tempo_yun=tempo_yun.split("'")
        '''
        print(eulero)
        print(velo)
        print(modalita)
        print(tempo_yun)
        print(e)
        
        '''
        '''
        print( a )
        print(b )
        print( c )
        print( e)
        '''
        yaw_1=eulero[1]
        roll_1=eulero[3]
        pitch_1=eulero[5]

        acc_Y=acc[1]
        acc_X=acc[3]
        acc_Z=acc[5]

        a_s=velo[1]
        a_d=velo[3]
        p_s=velo[5]
        p_d=velo[7]
        w_med=velo[9]

        motore=micros_radio[1]
        sterzo=micros_radio[3]

        d_motore=duty[1]
        d_sterzo=duty[3]
        t_yun=t_yun[0]

        yaw_rate_1=rate[5]
        roll_rate_1=rate[3]
        pitch_rate_1=rate[1]
        

        #modalita=str(modalita)
        #tempo_yun=str(tempo_yun)
        '''
        print(yaw_1)
        print(roll_1)
        print(pitch_1)
        print('///////////')
        print(acc_y)
        print(acc_x)
        print(acc_z)
        print('///////////')
        print(a_s)
        print(a_d)
        print(p_s)
        print(p_d)
        print(w_med)
        print('///////////')
        print(motore)
        print(sterzo)
        print('///////////')
        print(modalita)
        print('///////////')
        print(tempo_yun)
        '''    
        #print( '////////////////////    FINE   //////////////////' )

        yaw.set(yaw_1)
        pitch.set(pitch_1)
        roll.set(roll_1)

        yaw_rate.set(yaw_rate_1)
        pitch_rate.set(pitch_rate_1)
        roll_rate.set(roll_rate_1)

        ant_sin.set(a_s)
        ant_des.set(a_d)
        pos_sin.set(p_s)
        pos_des.set(p_d)
        w_media.set(w_med)


        acc_x.set(acc_X)
        acc_y.set(acc_Y)
        acc_z.set(acc_Z)

        
        duty_motore.set(d_motore)
        duty_sterzo.set(d_sterzo)

        
        tempo_yun.set(t_yun)
        mod.set(modalita[0])
        
        #print(w_target.get())
        
   
        ############# CREAZIONE BARRA ACC E STERZO #################

        ###### barra acceleratore##########
        if int(motore)>1500:
            x_motore=(int(motore)-1500)/5.17
            if x_motore<100 and x_motore>0:
                w.create_rectangle(600,y_acc,x_motore+ 600 ,y_acc+20 ,fill='green')
            if x_motore<100:
                w.create_rectangle(x_motore+600,y_acc,700 ,y_acc+20 ,fill='white')
                
        if int(motore)<1500:
            x_motore=(1500-int(motore))/5.11
            if x_motore<100 and x_motore>0:
                w.create_rectangle(600,y_acc,-x_motore+ 600 ,y_acc+20 ,fill='red')
            if x_motore<100:
                w.create_rectangle(500,y_acc,600-x_motore ,y_acc+20 ,fill='white')

         ##### barra sterzo###########################       
        if int(sterzo)>1472:
            x_sterzo=(int(sterzo)-1472)/3.93
            if x_sterzo<100 and x_sterzo>0:
                w.create_rectangle(600,y_eul,-x_sterzo+600 ,y_eul+20 ,fill='blue')
            if x_sterzo<100:
                w.create_rectangle(500,y_eul,600-x_sterzo,y_eul+20 ,fill='white')

        if int(sterzo)<1472:
            x_sterzo=(1472-int(sterzo))/4.36
            if x_sterzo<100 and x_sterzo>0:
                w.create_rectangle(600,y_eul,x_sterzo+ 600 ,y_eul+20 ,fill='blue')
            if x_sterzo<100:
                w.create_rectangle(x_sterzo+600,y_eul,700 ,y_eul+20 ,fill='white')

        #######################################################################################

     









        
    root.update()

            


######## funzione chiusura comunicazione#####


####################################### FINE FUNZIONE SSH ##############################################################��



