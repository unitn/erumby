#include <PWM.h>


#include <SoftwareSerial.h>
#define W_SOGLIA 1000
#define TEMPO_ATTESA_CANALE_A 14000 //in microsecondi!!!

SoftwareSerial mySerial_mega(10,9); // 10 ricevete 11 trasmittente
bool seriale;
unsigned int tempotras_1=0;
unsigned int tempotras_2=0;
unsigned int tempotras_3=0;
int prova[3];

int inizio=0;

int pinInt_1=2;
int pin_as=21; // anteriore sinsitra
int pin_ad=20; // anteriore destra
int pin_ps=19; // posteriore sinsitra
int pin_pd=18; // posteriore destra


int pin_A_as= 30;
int pin_A_ad= 32;
int pin_A_ps= 34;
int pin_A_pd= 36;

int pin_B_as= 47;
int pin_B_ad= 49;
int pin_B_ps= 51;
int pin_B_pd= 53;


float prova_inf=0.000005;


 double millis_float;
 double millis_float_as;
 double millis_float_ad;
 double millis_float_ps;
 double millis_float_pd;


float secondi;
float secondi_as;
float secondi_ad;
float secondi_ps;
float secondi_pd;

// w canale A
float w_A_as;
float w_A_ad;
float w_A_ps;
float w_A_pd;

//float w;
float w_as;
float w_ad;
float w_ps;
float w_pd;

int w_serial_as[4];
int w_serial_ad[4];
int w_serial_ps[4];
int w_serial_pd[4];
int w_serial_media[4];

float v;
float raggio=0.05;

unsigned int tempo0=0;
int tempo1=0;
int tempo2=0;


// variabili utlizzate dalle funzioni per il calcolo della velocità con interrupt
unsigned long  tempo0_as=0;
unsigned long  tempo1_as=0;
unsigned long  tempo2_as=0;

unsigned long  tempo0_ad=0;
unsigned long  tempo1_ad=0;
unsigned long  tempo2_ad=0;

unsigned long  tempo0_ps=0;
unsigned long  tempo1_ps=0;
unsigned long  tempo2_ps=0;

unsigned long  tempo0_pd=0;
unsigned long  tempo1_pd=0;
unsigned long  tempo2_pd=0;

// array velocità angolare
float w[4];


unsigned int w_serial[4];
float w_media;
int w_target=0;
int segno_serial[4];
int cont_safe1=1;


float w_int[4];



int segno[4];
unsigned int encoder_b[4];
unsigned int encoder_b_priv[4];

// variabili per il gestione dei comandi di sterzo e motore

int stato_motore=7020;
int stato_motore_in;

int stato_motore_int1;
int stato_motore_prec=1500;
int stato_sterzo=7020;
int stato_sterzo_in;

int stato_sterzo_int1;
int stato_sterzo_prec=1472;

// pin collegati al segnale di comando dello sterzo e della esc
int pin_sterzo=11;
int pin_motore=12;

// variabili per la lettura della ricevente
unsigned long  mic_prec_sterzo;
unsigned long  mic_sterzo;
unsigned long  mic_prec_motore;
unsigned long  mic_motore;

// costanti per il controllo PI  (non più utilizzate)
int kp=1;
int ki=1;



//int  kp_yun;
//int st;
//int mt;
//int st_1;
//int mt_1;



//int sterzo1;
//int sterzo2;
//int motore1;
//int motore2;

// array per la trasmissione dei valori di comando e duty ad arduino yun

int sterzo_trans[2];
int motore_trans[2];
int duty_sterzo_trans[2];
int duty_motore_trans[2];
int duty_controllo_trans[2];

// varibili per la modalità
int canale_3;
int modalita;




int com;

//variabili controllo spostamento
bool cont_spostamento=false;
int contatore_tacche_as;
int control1=0;
//int inizio=0;


// parametri controllo
float errore=0;
float errore_prec=0;
float errore_int=0;
float errore_der=0;

float comand;
float comand_prec=0;

int cont_safe;
int duty_controllo; 




int inizio_comando=2; // serve per permettere al veicolo di muoversi al prinìmo colpo
int freno=2;

 int rotazione_ruote=1; // serve per vedere se le ruote sono ferme
 int rotazione_ruote_2=1;

// variabili per filtrare la velocità in ingresso per evitare slittemaneti delle gomme in fase di partenza
float w_target_f=0;
float w_target_f_prec=0;

void setup() {
 
    //inizializzazione libreria PWM
InitTimersSafe();
SetPinFrequency(12, 71.45);
SetPinFrequency(11, 71.45);
 
// fine inizializzazione PWM
  
 

 
  
mySerial_mega.begin(57600);
  
  
//pinMode(pinInt_1,INPUT);
pinMode(pin_as,INPUT);
pinMode(pin_ad,INPUT);
pinMode(pin_ps,INPUT);
pinMode(pin_pd,INPUT);

pinMode(pin_A_as,INPUT);
pinMode(pin_A_ad,INPUT);
pinMode(pin_A_ps,INPUT);
pinMode(pin_A_pd,INPUT);

pinMode(pin_B_as,INPUT);
pinMode(pin_B_ad,INPUT);
pinMode(pin_B_ps,INPUT);
pinMode(pin_B_pd,INPUT);

 Serial.begin(115200); 




//// parte ricevente

attachInterrupt(1,motore,CHANGE); 
attachInterrupt(0,sterzo,CHANGE  ); 
 //tempo0=millis();
// w[0]=5;
//////// attivazione comunicazione seriale/////
seriale=true;

delay(500); // delay per sincronizzazione
pwmWriteHR(pin_motore, 6990);


}
//int stato_motore_int[20];
//int stato_sterzo_int[20];


int auto_1;
int control=0;
int inizio_controllo=0;


void loop() {

// st=0;
// mt=0;
// st_1=0;
// mt_1=0;
 
unsigned int a=millis();
 
 /// LETTURA ENCODER
 com=1;
 encoder();
 delay(5); //delay per far in modo di evitare i disturbi
  com=0;

//LETTURA CANALE A
 canale_3=analogRead(A8);
 /// se non è collegata la usb canale_3=analogRead(A8); a cuasa che cambiano i riferimenti e si maggiora il tutto di 10
 modalita=canale_3;
//Serial.print("canale 3 :");Serial.println(canale_3);

 





/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// //CON USB COLLEGATA
//if (canale_3 >60 && canale_3 <= 85){  
//                                      manuale();
//                                      cont_safe1=1;
//                                      modalita=1;
//                                    Serial.println("stato_normale");
//                                  }
//    else if ( canale_3 > 86)    { ricevente();
//                                  partenza();
//                                   modalita=2;
//                                   
//                                 
//                                   
//                                   
//                                   
//                                    if (stato_motore_int1>1800){//sicurezza();.
//                                     //controllo(20);
//                                      Serial.println("entrato nel controllo //////////////////");
//                                  // controllo(w_target);
//                                     }
//                          else {if (control1==1){   // duty neutro per sterzo e motore
////                                                     stato_motore=110;
////                                                     stato_sterzo=110;
//                            
//                                                   control1=0; 
//                                                 //   Timer1.pwm(pin_sterzo,stato_sterzo);
//                                                 //   Timer1.pwm(pin_motore,stato_motore);
//                                                              //if(duty_controllo > 110 ){Timer1.pwm(12,90);Serial.print("entrato posizione avanti");}
////                                                             if(duty_controllo < 110 ){Timer1.pwm(12,120);/*delay(20);*/ Timer1.pwm(12,110); Serial.print("entrato posizione retro");} // per evitare che vada in retro
////                                                            
//                                                           }
////                                
//                                                               
//                                               }
//                              
//                              
//                                 // duty neutro per sterzo e motore
////                                 stato_motore=110;
////                                 stato_sterzo=110;
//                        
//                                   }                                                       
//                
//        else    { //sicurezza();
//                   modalita=0;           
//                              } 
////                              
////     Serial.print(" partenza"); Serial.print(inizio_comando);
////     
////         Serial.print(" freno "); Serial.print(freno);
////         
////          Serial.print(" Inizio comando"); Serial.print(inizio_comando);
////         
//
////         
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////SENZA USB COLLEGATA
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (canale_3 >75 && canale_3 < 96){
                                      manuale();
                                      cont_safe1=1;
                                      modalita=1;
                                      
                                    // Serial.println("stato_normale");
                                  }
    else if ( canale_3 >105)    { ricevente();
                                  modalita=2;
                                  partenza();
                                    if (stato_motore_int1>1800){ // se l'acceleratore è premuto per più della metà si entra nella funzione del controllo
                                     controllo(w_target);
                                     }
                            else {if (control1==1){  // se nel ciclo precedente era attiva la funzione controllo si mette il motore e lo sterzo in posizione di neutro
                                                     pwmWriteHR(pin_motore,7020);
                                                     pwmWriteHR(pin_sterzo,6778);
                                                     w_target_f_prec=0;
                                                     inizio_controllo=0;
                                                     control1=0;
                                                          /*if(duty_controllo > 110 ){Timer1.pwm(12,90);Serial.print("entrato posizione avanti");}
                                                             if(duty_controllo < 110 ){Timer1.pwm(12,120);delay(20); Timer1.pwm(12,110); Serial.print("entrato posizione retro");} // per evitare che vada in retro
                                                             control1=0; Timer1.pwm(11,110);
                                                        

                                                        
                                                         */ 
                                                     comand_prec=0;
                                                     errore_prec=0;
                                                         }
//                                
                                                               
                                               }
                              
                                 
//                                 pwmWriteHR(pin_motore,7020);
//                                 pwmWriteHR(pin_sterzo,6778);
                        
                                   }                                                       
                
        else    { sicurezza();
                   modalita=0;           
                              } 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

 //int temp666=millis();
//Serial.print("tempo ricevente");Serial.println(temp666-temp66);                             
                              
                              
//COMUNICAZIONE SERIALE                              
com=1;
comun_seriale();
delay(5); //delay per far in modo di evitare i disturbi
com=0;

// delay per permettere di ricevere i valori del segnale della ricevente
//delay(10);

//Serial.println(mt);
unsigned int b=millis();
unsigned int tempo_es=b-a;

Serial.print("tempo esecuzione:  ");Serial.println(tempo_es);


// per mantere un tempo di campionamento pari a 40 millisecondi
if (tempo_es <40){ unsigned int del=40-tempo_es;
                 // Serial.println("entrato delay");
               delay(del);
                   }


 
 
Serial.println("/////////////////////////////////////////////////////////");



}


 




void a_s(){

  tempo1_as=micros();
 // Serial.println("tempo1_as");
 // Serial.println(tempo1_as);
 //   Serial.println("tempo0_as");
 // Serial.println(tempo0_as);
  
 tempo2_as=tempo1_as-tempo0_as;
 //    Serial.println("tempo2_as");
//  Serial.println(tempo2_as);
 millis_float_as=tempo2_as;
// Serial.println(millis_float_as);
 secondi_as=millis_float_as/1000000;
 // Serial.println( secondi_as);
 w_as=0.031415/((secondi_as));
 w_int[0]= w_as;
// v_as=w*raggio;
 tempo0_as=tempo1_as;
 rotazione_ruote= rotazione_ruote+1;
 
//Serial.print("A.S.  giro effettuato in "); Serial.print(tempo2_as); Serial.print("  millisecondi"); Serial.print("\t");  Serial.print("w :  "); Serial.print(w_as);Serial.print("  rad/s");Serial.print("\n");
 //if (cont_spostamento==true){contatore_tacche_as++;}
}

void a_d(){

tempo1_ad=micros();
 tempo2_ad=tempo1_ad-tempo0_ad;
 millis_float_ad=tempo2_ad;
 secondi_ad=millis_float_ad/1000000;
 w_ad=0.031415/((secondi_ad));
  w_int[1]= w_ad;
// v_as=w*raggio;

//Serial.print("A.D.  giro effettuato in "); Serial.print(tempo2_ad); Serial.print("  millisecondi"); Serial.print("\t");  Serial.print("w :  "); Serial.print(w_ad);Serial.print("  rad/s");Serial.print("\n");
 tempo0_ad=tempo1_ad;
}

void p_s(){

tempo1_ps=micros();
 tempo2_ps=tempo1_ps-tempo0_ps;
 millis_float_ps=tempo2_ps;
 secondi_ps=millis_float_ps/1000000;
 w_ps=0.031415/((secondi_ps));
 v=w_ps*raggio;
 
  w_int[2]= w_ps;

//Serial.print("P.S.  giro effettuato in "); Serial.print(tempo2_ps); Serial.print("  millisecondi"); Serial.print("\t");  Serial.print("w :  "); Serial.print(w_ps);Serial.print("  rad/s");Serial.print("\n");
 tempo0_ps=tempo1_ps;
}

void p_d(){

tempo1_pd=micros();
 tempo2_pd=tempo1_pd-tempo0_pd;
 millis_float_pd=tempo2_pd;
 secondi_pd=millis_float_pd/1000000;
 w_pd=0.031415/((secondi_pd));
  w_int[3]= w_pd;
// v_as=w*raggio;

//Serial.print("P.D.  giro effettuato in "); Serial.print(tempo2_pd); Serial.print("  millisecondi"); Serial.print("\t");  Serial.print("w :  "); Serial.print(w_pd);Serial.print("  rad/s");Serial.print("\n");
 tempo0_pd=tempo1_pd;
 rotazione_ruote_2= rotazione_ruote_2+1;
}

//int  stato_sterzo_int1_prec;

////ricevente
void sterzo(){ mic_sterzo=micros();
//          if (st==4){mic_sterzo=micros();
        stato_sterzo_in=mic_sterzo-mic_prec_sterzo;
          if( stato_sterzo_in <2100 && stato_sterzo_in >300  ){ if(com==0 ){ stato_sterzo_int1=stato_sterzo_in;
//                                                                                        if(abs(stato_sterzo_in-stato_sterzo_prec)<300){ stato_sterzo_int1=stato_sterzo_in;}
//                                                                                        else{stato_sterzo_int1=stato_sterzo_prec;}
                                                                                   
                                                                                     stato_sterzo_prec= stato_sterzo_int1;  
                                                                                      }
                                     
                                    //st++;
                                    }
        mic_prec_sterzo=mic_sterzo;

          }


void motore(){ mic_motore=micros();
         stato_motore_in=mic_motore-mic_prec_motore;
         if( stato_motore_in < 2100  ){ if(com==0 ){stato_motore_int1=stato_motore_in;
                                                   
                                                            }
                                      
                                         //  mt++;  
                                       }
       
     //   Serial.println("-- -- -- -- -- -- -- -- ");
       
        mic_prec_motore=mic_motore;
 }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
