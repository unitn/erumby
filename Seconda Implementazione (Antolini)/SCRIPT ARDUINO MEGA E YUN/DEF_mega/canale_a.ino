unsigned long int tempo_A_as;
unsigned long int tempo_A_ad;
unsigned long int tempo_A_ps;
unsigned long int tempo_A_pd;

double tempo_A_millis_as;
double tempo_A_millis_ad;
double tempo_A_millis_ps;
double tempo_A_millis_pd;

double tempo_A_app_as;
double tempo_A_app_ad;
double tempo_A_app_ps;
double tempo_A_app_pd;


 // ANTERIORE SINISTRA
float canale_A_as(){ 
  tempo_A_as=pulseIn(pin_A_as,HIGH,TEMPO_ATTESA_CANALE_A);

 if (tempo_A_as==0){w_A_as=0;/*Serial.print("w_A_as      ");Serial.println(w_A_as);*/}
  else{ tempo_A_app_as=tempo_A_as;
         tempo_A_millis_as=tempo_A_app_as/1000;
         w_A_as=31.415/tempo_A_millis_as;
    
    //segno rotazione
    
    encoder_b[0]=(digitalRead (pin_B_as));
    
    if (encoder_b[0]==0){segno[0]=1;}
     else{segno[0]=-1;}
     
   w_A_as=  w_A_as; 
 return w_A_as; 
  
   
    }
        }
        
        
//ANTERIORE DESTRA      
float canale_A_ad(){ 
  tempo_A_ad=pulseIn(pin_A_ad,HIGH,TEMPO_ATTESA_CANALE_A);
 if (tempo_A_ad==0){w_A_ad=0;}
  else{
        
        tempo_A_app_ad=tempo_A_ad;
         tempo_A_millis_ad= tempo_A_app_ad/1000;
  
         w_A_ad=31.415/tempo_A_millis_ad;
         
         
            encoder_b[1]=(digitalRead (pin_B_ad));
    
    if (encoder_b[1]==0){segno[1]=-1;}
     else{segno[1]=1;}
     
   w_A_ad=  w_A_ad; 
        return w_A_ad;
      }
        }
     
     
//POSTERIORE SINISTRA        
float canale_A_ps(){ 
  tempo_A_ps=pulseIn(pin_A_ps,HIGH,TEMPO_ATTESA_CANALE_A);
 if (tempo_A_ps==0){w_A_ps=0;} 
  else{  tempo_A_app_ps=tempo_A_ps;
         tempo_A_millis_ps= tempo_A_app_ps/1000;
         w_A_ps=31.415/tempo_A_millis_ps;
         
          encoder_b[2]=(digitalRead (pin_B_ps));
    
    if (encoder_b[2]==0){segno[2]=1;}
     else{segno[2]=-1;}
     
   w_A_ps=  w_A_ps; 

   return w_A_ps;
      }
        }
        
 // POSTERIORE DESTRA
 
  float canale_A_pd(){ 
  tempo_A_pd=pulseIn(pin_A_pd,HIGH,TEMPO_ATTESA_CANALE_A);
 if (tempo_A_pd==0){w_A_pd=0;}
  else{  tempo_A_app_pd=tempo_A_pd;
         tempo_A_millis_pd=tempo_A_app_pd/1000;
         w_A_pd=31.415/tempo_A_millis_pd;
         encoder_b[3]=(digitalRead (pin_B_pd));
    
    if (encoder_b[3]==0){segno[3]=-1;}
     else{segno[3]=1;}
     
   w_A_pd= w_A_pd;
           
      return w_A_pd ;
      }
        }
        
