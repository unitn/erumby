#  - **University of Trento** - 
##_Mechatronic Engineering -  E-Rumby Project_

###_**Pagina di progetto**_

Benvenuti nella pagina del progetto di **E-Rumby** 

Lo scopo di questo documento è la presentazione del progetto E-Rumby, ossia la sensorizzazione e la messa in opera di un modello di macchina con motore elettrico.

Al momento il progetto risulta completato nelle due parti di cui si componte:

 * *Planchestainer Jacopo*, modellazione longitudinale e controllore longitudinale di velocità;
 * *Vesco Mattia*, integrazione di un sensore visivo frontale per l'inseguimento di traiettoria in autonomo, basi per lo studio della geometria della traiettoria.

All'interno della terza implementazione del sistema si trovano le tesi e l'ultima versione del firmware funzionante (commentato).

![erumby](https://bytebucket.org/unitn/erumby/raw/b0ab7f07284f4c7ff4c0f939af31957f4b4bf9b1/Terza%20Implementazione%20%28Vesco%2CPlanchestainer%29/Tesi/eRumby_finito.jpg)