  #!/usr/bin/python

import sys    
import os
import serial
import time


#os.environ["PYTHONUNBUFFERED"] = "1" #per eliminare il buffer

s=serial.Serial("/dev/ttyATH0",115200,timeout=2)


s.flushInput()



velocita='05'
kp='23'
ki='05'

#0.23

#valore decimale del kp

nome_prova='prova_controllo_w_target_'+ velocita+'_Kp_' +kp +'_'+'_Ki_' +ki +'_'+'.txt'
#nome_prova='avanti_frenata_4.txt'
#nome_prova='curvette.txt'
#nome_prova='113.txt'
#nome_prova='controllo_20_inversione.txt'

#nome_prova='109_.txt'
#nome_prova='97_.txt'
#nome_prova='107_.txt'
#nome_prova='106_.txt'
#nome_prova='105_.txt'
#nome_prova='104_.txt'
#nome_prova='103_.txt'




file_txt=open(nome_prova,"w")

data_ora = time.asctime( time.localtime(time.time()) )


#intestazione_documento

file_txt.write("#PROVA :  " )
file_txt.write(nome_prova)
file_txt.write("\t")
file_txt.write("\n#")
file_txt.write(data_ora)
file_txt.write("\n#")
file_txt.write("Unita di misura: \n#")
file_txt.write("- angoli espressi in gradi")  
file_txt.write("\n#")
file_txt.write("- accelerazioni espresse in g X senso di marcia, Y laterale, Z verticale")
file_txt.write("\n#")
file_txt.write("- velocita ruote in rad/s")
file_txt.write("\n#")
file_txt.write("- valori sterzo e motore epressi in micorsecondi letti dalla ricevente (tempo in cui l'onda quadra ha valore massimo)")
file_txt.write("\n#")
file_txt.write("- range motore: neutro: 1496  velocita max: 2020  frenata/retromarcia max: 976")
file_txt.write("\n#")
file_txt.write("- range sterzo: neutro: 1472 destra: 1048 sinistra: 1892")
file_txt.write("\n#")
file_txt.write("- duty sterzo e motore percentuale dutycicle espressa tra 0 e 1023, periodo onda 14 millisecondi")
file_txt.write("\n#")
file_txt.write("- tempi di esecuzione in millisecondi")
file_txt.write("\n#")

file_txt.write("velocita target controllo :  ")
file_txt.write(velocita)
file_txt.write("\n#")
file_txt.write("costante proporzionale :  ")
file_txt.write(str(float(kp)/100))
file_txt.write("\n#")
file_txt.write("costante integrativa :  ")
file_txt.write(str(float(ki)/100))
file_txt.write("\n#")


file_txt.write("\t[ms] \t\t[gradi] [gradi] [gradi] \t " )
file_txt.write("[g] \t [g] \t [g] \t " )
file_txt.write("\t[rad/s] \t [rad/s] \t [rad/s] \t  [rad/s] \t [rad/s]"  )
file_txt.write( "\t [ms] \t\t [ms] \t \t [/] \t\t   [%] \t\t   [%]  \t  [ms] \t\t\t    " )
file_txt.write( "\n#" )



file_txt.write(" tempo_yun_long \t" )
file_txt.write("yaw \t roll \t pitch \t\t " )
file_txt.write("acc Y \t acc X \t acc Z \t " )
file_txt.write("\t ant_sin \t ant_des \t post_sin \t post_des \t w_media"  )
file_txt.write( "\t motore \t sterzo \t modalita \t duty_motore \t duty_sterzo  \t tempo_python          " )
file_txt.write( "\n" )

a='1'

tempoprec=time.time()
stato=55
f=0
t=0

h=0
	
s.write(a)
s.write(velocita)
s.write(kp)
s.write(ki)

#s.write(a)

while True:
	tempo0=time.time()
	
	
	#print s.inWaiting() 
	#while s.inWaiting()< 2 :
	#	c=0


	if s.inWaiting()> 1:


		
		t=s.inWaiting()
	
		print "//////  entrato  /////"
		print s.inWaiting() 
		tempo1=time.time()
      		  
                
			
        
		
	
        	
	
		
		eulero_1= s.read(7)
		
		print(eulero_1)

        	eulero_2= s.read(7)

		eulero_3= s.read(7)

	
	


	
		acc_1= s.read(7)

	
		acc_2= s.read(7)

		acc_3= s.read(7)

		tempo10=time.time()


      	 	ant_sin= s.read(7)

      		ant_des= s.read(7)

		post_sin= s.read(7)

		post_des= s.read(7)

        	w_media= s.read(7)

		tempo11=time.time()



		motore= s.read(4)
		#print(motore)

        	sterzo= s.read(4)

		modalita=s.read(1)


		duty_motore=s.read(3)

		duty_sterzo=s.read(3)

		#tempo_yun=s.read(2)

		tempo_yun_long=s.read(8)


		#s.flushInput()


		s.write(a)



		#tempo_yun=str(int(tempo_yun)-100)


		#scrittura della velocita del controllo


		tempo12=time.time()
 		#print 'tempo lettura delle altre cose'
		#print tempo12-tempo11


		#scrive il conrollo		



		#print'tempo yun'
		#print(tempo_yun)





		#scrive il conrollo
        	#s.write(a)


		#SCRITTURA VALORI

		#s.write(velocita)
		#s.write(kp)






		modalita_int=int(modalita)

	
		if modalita_int==0:
			stato='safe'
			
		elif modalita_int==1:
			stato='manu'
		elif modalita_int==2:
			stato='auto'



		if int(duty_motore) > 200:
			duty_motore=str(int(duty_motore)/10)

		if int(duty_sterzo) > 200:
			duty_sterzo=str(int(duty_sterzo)/10)
	
		#print(duty_motore)
		#print(duty_sterzo)

	
        	print (eulero_1 ,eulero_2 ,eulero_3 ,		acc_1 ,acc_2 , acc_3)
		#print (eulero_1 ,eulero_2 ,eulero_3 )

		tempo9=time.time()

       		print(ant_sin  ,ant_des , post_sin, post_des ,w_media )

		print(stato )


	

		tempo3=time.time()

		#print "tempo stampa"

		#print tempo3-tempo9

		tempo=str(int((tempo3-tempo1)*1000))
		#print(tempo_yun_long)

		#print (acc_1 ,acc_2 , acc_3)
	
		#print canale_3

		#s.flushInput()
		#tempo8=time.time()
		file_txt.write("\t")
 		file_txt.write(tempo_yun_long)
		file_txt.write("\t")
       	 	file_txt.write(eulero_1)
		file_txt.write("\t")
        	file_txt.write(eulero_2)
 		file_txt.write("\t")
 		file_txt.write(eulero_3)
 		file_txt.write("\t")

		file_txt.write("\t")
       	 	file_txt.write(acc_1)
		file_txt.write("\t")
        	file_txt.write(acc_2)
 		file_txt.write("\t")
 		file_txt.write(acc_3)
 		file_txt.write("\t")

		file_txt.write("\t")
        	file_txt.write(ant_sin)
		file_txt.write("\t")
		file_txt.write("\t")
        	file_txt.write(ant_des)
 		file_txt.write("\t")
		file_txt.write("\t")
 		file_txt.write(post_sin)
 		file_txt.write("\t")
		file_txt.write("\t")
       	 	file_txt.write(post_des)
 		file_txt.write("\t")
 		file_txt.write("\t")

 		file_txt.write(w_media)
 		file_txt.write("\t")
		file_txt.write("\t")


        	file_txt.write(motore)

 		file_txt.write("\t")
 		file_txt.write("\t")
 		file_txt.write(sterzo)
 		file_txt.write("\t")
 		file_txt.write("\t")
 		file_txt.write(stato)
 		file_txt.write("\t")
 		file_txt.write("\t")
 
 		file_txt.write("\t")
 
 
 		file_txt.write(duty_motore)
 		file_txt.write("\t")
 		file_txt.write("\t")

		file_txt.write(duty_sterzo)
 		file_txt.write("\t")
 		file_txt.write("\t")
 		#file_txt.write(tempo_yun)
 		file_txt.write("\t")


 		file_txt.write( tempo)
 		file_txt.write("\t")
 		file_txt.write("\t")

 		#file_txt.write("\t")
 		#file_txt.write("\t")
 		#file_txt.write(str(t))
 	
		tempo9=time.time()
	
		print 'tempo dopo lettura'
		print tempo9-tempo12



 		

		tempo4=time.time()
		print "tempo ciclo lettura"
		print tempo4-tempoprec
 		print(tempo4-tempo1)
		

 		#file_txt.write("\t")
 		#file_txt.write(str((tempo4-tempoprec)*1000))
		file_txt.write("\n")

		tempoprec=tempo4;


	#else:
		#s.write(a)

	
	
	#tempo00=time.time()

	
	#print "ciclo a vuoto  /////"
 	#print(tempo00-tempo0)
	

	#s.write(a)
	
	#s.flushOutput()

	

file_txt.close()	
s.close()
