
void manuale() {
  ricevente();

  // pre frenare il veicolo
  if (stato_motore > 111 && freno == 2) {
    Timer1.pwm(pin_motore, stato_motore);
    delay(20);
    freno = 1;
    Timer1.pwm(pin_motore, 110);
  }

  // per non ripetere la frenata o andare in retromarcia
  if (stato_motore > 110 && freno == 1) {
    Timer1.pwm(pin_motore, 110);
  }

  //per far avanzare il veicolo avanti il veicolo

  if (stato_motore < 110) {
    partenza(); // non è possibile metterla nella modalità perchè va eseguita con il veicolo acceso
    Timer1.pwm(pin_motore, stato_motore);
    freno = 2;
  }

  Timer1.pwm(pin_sterzo, stato_sterzo);

}



