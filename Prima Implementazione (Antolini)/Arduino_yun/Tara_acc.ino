
//costanti taratura acc
//#define Z_TARA_LOW 253
//#define Z_TARA_HIGH 257
//#define Y_TARA_LOW 253
//#define Y_TARA_HIGH 271
//#define X_TARA_LOW 255
//#define X_TARA_HIGH 270


int zero[3];
float risoluzione[3];





void set_up_taratura(){
      
      int intervallo_x= abs(ACCEL_X_MAX)+abs(ACCEL_X_MIN);
      int intervallo_y= abs(ACCEL_Y_MAX)+abs(ACCEL_Y_MIN);
      int intervallo_z= abs(ACCEL_Z_MAX)+abs(ACCEL_Z_MIN);
      
    risoluzione[0]=0.003831418;                   //2/intervallo_x;
    risoluzione[1]= 0.003780718;                  //2/intervallo_y;
    risoluzione[2]= 0.003875969;                 //2/intervallo_z;
    
    zero[0]=ACCEL_X_MAX-(intervallo_x/2);
    zero[1]=ACCEL_Y_MAX-(intervallo_y/2);
    zero[2]=ACCEL_Z_MAX-(intervallo_z/2);
 //  Serial.print(zero[0]             );//Serial.print(zero[0]);Serial.print(zero[0]); 
    
    
    }
    
    
    
   float tara_acc(float* dato, int i){
   
       float dato_tara;
      if (dato[i] > 0 && zero[i] > 0){dato_tara= (dato[i]  - zero[i])*risoluzione[i];} 
           else if (dato[i]  > 0 && zero[i] < 0){dato_tara= (dato[i]  + abs(zero[i]))*risoluzione[i];}
               else if (dato[i]  < 0 && zero[i] > 0){dato_tara= (dato[i]  - zero[i])*risoluzione[i];}                      /// - considerando che lo bisgna sommare ad un numero negativo
                   else  {dato_tara= (dato[i]  + zero[i])*risoluzione[i];}  // caso (dato < 0 && zero < 0)
      
      return dato_tara;
   
   
     }
     
     
   
