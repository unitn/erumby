clc
close all
clear all

% Definizione I/O
a0 = 0;            % TSL1401's analog output
si = 4;            % TSL1401's SI pin.
clk = 7;           % TSL1401's CLK pin.

% Variabili per start/stop
PIXEL_CNT = 128;
exposure = 8333;
%Variabili di output
out = '';
pdata=zeros(PIXEL_CNT);     % array che contine i dati in ingresso dal sensore
background=zeros(PIXEL_CNT);  %array che contine il background bianco
%Variabili per i conti
minimi=zeros(PIXEL_CNT);         %se trova pi� di un valore di minimo 
dimminimi=1;          %dimensione effettivadel vettore di minimi
minimo=0;             %mediadei punti di minimo trovati
minimibg=zeros(PIXEL_CNT);         %se trova pi� di un valore di minimo 
dimminimibg=1;        %dimensione effettivadel vettore di minimi
minimobg=0;           %media dei punti di minimo trovati
res=zeros(PIXEL_CNT);

%Setup
a=arduino('com8')
configureDigitalPin(a,si,'Output');
configureDigitalPin(a,clk,'Output');
pause on;

%GetBg
      %Mando un CLK e un SI per iniziare la letture.
      writeDigitalPin(a,si, 1);
      writeDigitalPin(a,clk, 1);
      pause(10*10^(-6));
      writeDigitalPin(a,si,0);
      writeDigitalPin(a,clk, 0);                 
      %Ripulisco i dati "vecchi".
      for i=1:PIXEL_CNT
        writeDigitalPin(a,clk,1);
        writeDigitalPin(a,clk,0);
      end
      %Ritardo per lasciare il tempo di esposizione
      pause(exposure*10^(-9));
      %Mando un CLK e un SI per iniziare la letture.
      writeDigitalPin(a,si, 1);
      writeDigitalPin(a,clk, 1);
      pause(10*10^(-6));
      writeDigitalPin(a,si,0);
      writeDigitalPin(a,clk, 0);                    
      for i = 1:PIXEL_CNT
        writeDigitalPin(a,clk,1);
        background(i) = (readVoltage(a,a0)/4);
        writeDigitalPin(a,clk,0);
      end
      display('Background acquisito!');

      display('Inizio loop!');
%Loop
while (1)
  %getPix
      %Mando un CLK e un SI per iniziare la letture.
      writeDigitalPin(a,si, 1);
      writeDigitalPin(a,clk, 1);
      pause(10*10^(-6));
      writeDigitalPin(a,si,0);
      writeDigitalPin(a,clk, 0);
      %Ripulisco i dati "vecchi".
      for i=1:PIXEL_CNT
        writeDigitalPin(a,clk,1);
        writeDigitalPin(a,clk,0);
      end
      %Ritardo per lasciare il tempo di esposizione
      pause(exposure*10^(-9));
      %Mando un altro CLK e SI per iniziare la lettura effettiva
      writeDigitalPin(a,si, 1);
      writeDigitalPin(a,clk, 1);
      pause(10*10^(-6));
      writeDigitalPin(a,si,0);
      writeDigitalPin(a,clk, 0);                    
      %Faccio la lettura dei dati reali inviando un CLK e leggendo il relativo dato
      for i = 1:PIXEL_CNT
        writeDigitalPin(a,clk,1);
        pdata(i) = (readVoltage(a,a0)/4);
        res(i) = (readVoltage(a,a0)/4)-background(i);    %uso solo 256 livelli al posto di 1024 (filtro gi� un po' di disturbi) e sottraggo lo sfondo
        writeDigitalPin(a,clk,0);
      end
    %FindMinimi
        minimo = pdata(1);
        for i=1:PIXEL_CNT
            if (minimo>pdata(i))
                minimo=pdata(i);
            end
        end
        dimmedia = 0;
        media=0;
        for i=1:PIXEL_CNT
            if (((pdata(i))>(minimo-4))&&(pdata(i)<(minimo+4)))
                minimi(dimminimi) = i;
                dimminimi=dimminimi+1;
                media=media+i;
                dimmedia=dimmedia+1;
            end
        end
        minimo=media/dimmedia; 
    %FindMinimiBg
        minimo = res(1);
        for i=1:PIXEL_CNT
            if (minimo>res(i))
                minimo=res(i);
            end
        end
        dimmedia = 0;
        media=0;
        for i=1:PIXEL_CNT
            if (((res(i))>(minimo-4))&&(res(i)<(minimo+4)))
                media=media+i;
                minimibg(dimminimibg)=i;
                dimminimibg=dimminimibg+1;
                dimmedia=dimmedia+1;
            end
        end
        minimobg=media/dimmedia;
    %DispPix
        figure(1)
        subplot(1,2,1)
        cla
        grid on
        hold on
        stairs(pdata);
        stairs(minimi(1,:),'r');
        stairs(minimibg(1,:),'k');
        plot(minimo,0,'*g');
        plot(res(i),'c');
        hold off
        subplot(1,2,2)
        hold on
        subimage(pdata)
        display('Printing')
        %reset variabili
        dimminimi=1;          %dimensione effettivadel vettore di minimi
        minimo=0;             %mediadei punti di minimo trovati
        dimminimibg=1;        %dimensione effettivadel vettore di minimi
        minimobg=0;           %media dei punti di minimo trovati
end


