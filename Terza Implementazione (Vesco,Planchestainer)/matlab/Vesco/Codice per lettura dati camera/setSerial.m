function [ s,flag] = setSerial( comPort)
%apertura comunicazione seriale con Arduino
flag = 1;
s = serial(comPort);
set(s,'DataBits',8);
set(s,'StopBits',1);
set(s,'BaudRate',9600);
set(s,'Parity','none');
set(s,'InputBufferSize',1024)
fopen(s);
end

