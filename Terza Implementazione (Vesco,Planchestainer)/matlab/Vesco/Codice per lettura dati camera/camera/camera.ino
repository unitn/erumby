#include <stdio.h>
#include <math.h>

#define PIXEL_CNT 128
#define C 2                  //numero del foro al quale è fissata la camera, 1 basso, 5 alto
#define BETA 40               //angolo di inclinazione della camera
#define K 5                  //guadagno del controllore delle ruote
#define A 2                   //distanza tra il cdm della macchina e l'assale posteriore
#define L 2                   //distanza tra i due assali della macchina

//Definizione costanti
double b = 0;
double c = 0;
// Definizione I/O
const int a0 = 0;            // TSL1401's analog output
const int si = 7;            // TSL1401's SI pin.
const int clk = 8;           // TSL1401's CLK pin.


// Variabili per start/stop
int e = 0;

// Variabili di output
String out = "";
int pdata[PIXEL_CNT];     // array che contine i dati in ingresso dal sensore
int background[PIXEL_CNT];  //array che contine il background
int res[PIXEL_CNT];        //array con la differenza tra dati in ingresso e background

//Variabili per i conti
typedef struct Minimo {    //struttura che contiene il punto di minimo in valore e posizione
  int valore;
  int posizione;
};

typedef struct Minimi {   //Struttura che contiene tutti i punti nell'intorno del minimo
  int valore[PIXEL_CNT];
  int posizione[PIXEL_CNT];
  int n;
};

Minimo punto;              //punto di minimo
Minimi punti;              //intorno del minimo
Minimo puntobg;            //punto di minimo con la Background Subtraction
Minimi puntibg;            //intorno del minimo con la Background Subtraction

// Inizializzazione
void setup() {
  Serial.begin(19200);         //comunicazione seriale
  pinMode(si, OUTPUT);        //pin di uscita per si
  pinMode(clk, OUTPUT);       //pin di uscita per clk
  pinMode(13, OUTPUT);        //led della board
  //inizializzazione dei led
  switch (C) {
    case 1:
      c = 127.27 + 25.83 * cos(BETA);
      break;
    case 2:
      c = 144.27 + 25.83 * cos(BETA);
      break;
    case 3:
      c = 161.27 + 25.83 * cos(BETA);
      break;
    case 4:
      c = 178.27 + 25.83 * cos(BETA);
      break;
    case 5:
      c = 195.27 + 25.83 * cos(BETA);
      break;
  }
  b = c * tan(BETA);      //distanza in avanti alla quale la camera acquisisce campioni
}

void loop() {
  punti.n = 0;                 //svuoto gli intorni dei minimi
  puntibg.n = 0;
  int s = Serial.read();       //lettura dei comandi da seriale
  if (s == 'B')                //'B'=Partenza della lettura
  {
    e = s;
    digitalWrite(13, HIGH);
  }
  if (s == 'E')                //'E'=Fine delle letture
  {
    e = s;
    digitalWrite(13, LOW);
  }
  if (s == 'G')                //'G'=Acquisisci il background, ma non partire
  {
    getBG(5000);
    digitalWrite(13, HIGH);
  }

  //ripeto il tutto per avere un ciclo infinito
  if (e == 'E')
  {
    e = s;
    digitalWrite(13, LOW);
  }
  if (e == 'B')
  {
    getPix(5000);            //funzione per l'acquisizione
    //FindMinimi();            //algoritmo base per trovare il minimo e il suo intorno
    FindMinimiBg();          //algoritmo di Background Subtraction per trovare il minimo e il suo intorno
    //FindDistance();          //algoritmo per capire in che direzione sterzare
    //dispPix();             //Scrive sulla seriale tutti i valori letti dalla cam
    dispMin();               //Scrive sulla seriale solo il minimo trovato con i due algoritmi
  }
}

// Funzioni
// getBG
// Acquisizione dei dati della camera per il background, exp = esposizione in uS
void getBG(int expose)
{
  //Mando un CLK e un SI per iniziare la letture.
  clockSI(si, clk);
  //Ripulisco i dati "vecchi".
  for (int i = 0; i < PIXEL_CNT; i++) {
    digitalWrite(clk, HIGH);
    digitalWrite(clk, LOW);
  }
  //Ritardo per lasciare il tempo di esposizione
  delayMicroseconds(expose);
  //Mando un altro CLK e SI per iniziare la lettura effettiva
  clockSI(si, clk);
  //Faccio la lettura dei dati reali inviando un CLK e leggendo il relativo dato
  char tbs[4];
  for (int i = 0; i < PIXEL_CNT; i++) {
    digitalWrite(clk, HIGH);
    background[i] = analogRead(a0) / 4;  //uso solo 256 livelli al posto di 1024
    sprintf(tbs, "%3d ", background[i]);
    out += tbs;
    digitalWrite(clk, LOW);
  }
  Serial.println(out);
  Serial.println("Background acquisito!");
  out = "";
}

// getPix
// Acquisizione dei dati della camera, exp = esposizione in uS
void getPix(int expose)
{
  //Mando un CLK e un SI per iniziare la letture.
  clockSI(si, clk);
  //Ripulisco i dati "vecchi".
  for (int i = 0; i < PIXEL_CNT; i++) {
    digitalWrite(clk, HIGH);
    digitalWrite(clk, LOW);
  }
  //Ritardo per lasciare il tempo di esposizione
  delayMicroseconds(expose);
  //Mando un altro CLK e SI per iniziare la lettura effettiva
  clockSI(si, clk);
  //Faccio la lettura dei dati reali inviando un CLK e leggendo il relativo dato
  for (int i = 0; i < PIXEL_CNT; i++) {
    digitalWrite(clk, HIGH);
    pdata[i] = (analogRead(a0) / 4);
    res[i] = pdata[i] - background[i];  //uso solo 256 livelli al posto di 1024 (filtro giÃ  un po' di disturbi) e sottraggo lo sfondo
    digitalWrite(clk, LOW);
  }
}

//clockSI è una funzione che manda un SI e un CLK per avvertire il sensore che inizio una lettura
void clockSI(int dataPin, int clockPin) {
  digitalWrite(dataPin, HIGH);
  digitalWrite(clockPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(dataPin, LOW);
  digitalWrite(clockPin, LOW);
}

//dispPix è la funzione per la visualizzazione dei dati sulla porta seriale
//protocollo di comunicazione:  "dato1 dato2 ... dato128"
void dispPix()
{
  char tbs[4];
  for (int i = 0; i < PIXEL_CNT; i++) {
    sprintf(tbs, "%3d ", pdata[i]);
    out += tbs;
  }
  Serial.println(out);
  out = "";
}
//dispPix è la funzione per la visualizzazione dei minimi sulla porta seriale
//protocollo di comunicazione:  "min minbg"
void dispMin()
{
  float diff = ((float)(punto.posizione - 64)) * (c / 128.0);   //distanza tra i due centri in mm
  //Serial.print(punto.posizione);
  Serial.print(punto.valore);
  Serial.print('\t');
  Serial.print(punto.posizione);
  Serial.print('\t');
  Serial.println(diff);
  out = "";
}



//IPOTESI  c'è una sola linea, in caso contrario non va!!!
void FindMinimi()
{
  //cerco il minimo
  punto.valore = pdata[0];
  punto.posizione = 0;
  for (int i = 0; i < PIXEL_CNT; i++) {
    if (punto.valore > pdata[i]) {
      punto.valore = pdata[i];
      punto.posizione = i;
    }
  }
  //cerco l'intorno
  int media = 0;
  int posmedia = 0;
  for (int i = 0; i < PIXEL_CNT; i++) {
    if (((pdata[i]) > (punto.valore - 4)) && (pdata[i] < (punto.valore + 4))) {
      punti.valore[punti.n] = pdata[i];
      punti.posizione[punti.n] = i;
      punti.n++;
      media += pdata[i];
      posmedia += i;
    }
  }
  punto.valore = media / punti.n;
  punto.posizione = posmedia / punti.n;
}

void FindMinimiBg()
{
  //cerco il minimo
  puntobg.valore = res[0];
  puntobg.posizione = 0;
  for (int i = 10; i < PIXEL_CNT - 10; i++) {
    if (puntobg.valore > res[i]+4) {
      puntobg.valore = res[i];
      puntobg.posizione = i;
    }
  }
  //cerco l'intorno
  if (puntobg.posizione != 0) {
    int media = 0;
    int posmedia = 0;
    for (int i = 10; i < PIXEL_CNT - 10; i++) {
      if (((res[i]) > (puntobg.valore - 4)) && (res[i] < (puntobg.valore + 4))) {
        puntibg.valore[puntibg.n] = res[i];
        puntibg.posizione[puntibg.n] = i;
        puntibg.n++;
        media += res[i];
        posmedia += i;
      }
    }
    if ((posmedia / puntibg.n >= 10) && (posmedia / puntibg.n <= 117)) {
      punto.valore = media / puntibg.n;
      punto.posizione = posmedia / puntibg.n;
    }
  }
}

/*
void FindDistance()
{
  int middle = 64;                       //definisco il punto centrale della linea
  int diff = punto.posizione-middle;     //distanza in pixel tra il centro "visto" e quello della macchina
  double gamma = atan(diff/c);           //angolo tra i due punti precedenti

  if (diff<4){
    digitalWrite(left,HIGH);
    digitalWrite(right,LOW);
    myservo.write(90+K*gamma);
    double raggio = square((A2*A2)+(L*L)*pow(tan(gamma)*tan(gamma),-1));   //raggio istantaneo della curva, gamma è da sostituire con l'angolo delle ruote
    return;
  }
  if (diff>4){
    digitalWrite(right,HIGH);
    digitalWrite(left,LOW);
    myservo.write(90-K*gamma));
    double raggio = square((A2*A2)+(L*L)*pow(tan(gamma)*tan(gamma)),-1);    //raggio istantaneo della curva, gamma è da sostituire con l'angolo delle ruote
    return;
  }
  digitalWrite(left,LOW);
  digitalWrite(right,LOW);
  myservo.write(90);
  return;
}*/





