clear all
close all
clc
pause on

%inizializzo le strutture usate
minimi=zeros(128,1);                         %0 se non c'� nulla, 255 se � nel range del punto di minimo
minimibg=zeros(128,1);                       %uguale a prima, ma usando la background subtraction
%inizializzo la comunicazione
s=setSerial('COM9');                   
pause(2)
%comando per acquisire il background 
fprintf(s,'%c','G');
b=fgetl(s);
%salvo il background
back(:,1)=str2num(b);
display(fgetl(s));
pause(5)
%comando per iniziare l'acquisizione
fprintf(s,'%c','B');
pause(1)
figure()
%quanti sono i dati "buoni" rispetto a tutti i cilci che fa
k=0;
%lo script gira per n volte e legge solo se ha tutti i dati in ingresso
for i=1:50000
    i
    %numero di dati in ingresso
    if (s.BytesAvailable>=512)    
        b=fgetl(s)
        k=k+1;
        %elimino i dati vecchi e mantengo solo gli ultimi 120 campioni,
        %serve a velocizzare la visualizzazione
        if (k>120)
            data(:,1)=[];
            res(:,1)=[];
            img(:,1)=[];
            minimo(:,1)=[];
            minimobg(:,1)=[];
            k=120;
        end
        data(:,k) = str2num(b);
        res(:,k) = data(:,k)-back(:,1);
        img(:,k)=(data(:,k));
        img(:,k)=img(:,k)/255;
        %ricerca minimo globale con entrambi gli algoritmi
        minimo(1,k)=data(1,k);
        minimo(2,k)=1;
        minimobg(1,k)=res(1,k);
        minimobg(2,k)=1;

        for j=1:size(data,1)
            %normale
            if (minimo(1,k)>data(j,k)) 
                minimo(1,k)=data(j,k);
                minimo(2,k)=j;
            end
            %background subtraction
            if (minimobg(1,k)>res(j,k)) 
                minimobg(1,k)=res(j,k);
                minimobg(2,k)=j;
            end
        end
        
        %trovo i punti all'interno del range di minimo e correggo il minimo
        %mediando
        media=0;
        mediabg=0;
        lbg=0;
        l=0;
        minimi=zeros(128,1);
        minimibg=zeros(128,1);
        
        %ricerca range di confidenza
        for j=1:size(data,1)
            %normale
            if ((data(j,k)>(minimo(1,k)-3))&&(data(j,k)<(minimo(1,k)+3)))
                minimi(j,1)=255;
                media=media+j;
                l=l+1;
            end
            %background subtraction
            if ((res(j,k)>(minimobg(1,k)-3))&&(res(j,k)<(minimobg(1,k)+3)))
                minimibg(j,1)=255;
                mediabg=mediabg+j;
                lbg=lbg+1;
            end
        end
        minimo(2,k)=round(media/(l));
        minimo(1,k)=data(minimo(2,k));
        minimobg(2,k)=round(mediabg/(lbg));
        minimobg(1,k)=data(minimobg(2,k));
        
        
        %codice plot
        subplot(1,2,1)
        cla
        grid on
        hold on
        xlim([0 128]);
        ylim([-260 260]);
        xlabel('n� of Pixel')
        ylabel('Digital Value')
        stairs(data(:,k));
        stairs(minimi(:,1),'r');
        stairs(minimibg(:,1),'k');
        plot(minimo(2,k),0,'*g');
        plot(minimobg(2,k),0,'*y');
        plot(res(:,k),'c');
        legend('Ingresso Camera','Minimi base','Minimi Subtraction','Punto Medio Normale','Punto Medio Subtraction','Subtraction');
        hold off
        subplot(1,2,2)
        hold on
        subimage(img(:,:))
        plot(1:k,minimo(2,1:k),'*g');
        plot(1:k,minimobg(2,1:k),'*y');
        ylabel('n� of Pixel')
        xlabel('Time')
        hold off
        pause(0.0001)    
    end
end
display('Finito!')
fprintf(s,'%c','E');
pause(1);
fclose(s);