#include "pins_arduino.h"
#include "Arduino.h"
#include "inttypes.h"

#define ISR_HOW ISR_NOBLOCK //ISR_BLOCK  ISR_NOBLOCK
#define BAUD 19200

/*---------------------------------------------------------------------------
 * Receiver pin definitions
 * To pick your own PCINT pins look at page 2 of Atmega 328 data sheet and
 * match Atmega vs Arduino pinout looking also at the Duemilanove data sheet
 * These pins need to correspond to the TCHPIN, AUXPIN, THRPIN, STRPIN below
 * Pin 4=20, Pin 5=21, Pin 6=22, Pin 7=23
 
 * Theory: all IO pins on Atmega168/328 are covered by Pin Change Interrupts.
 * The PCINT corresponding to the pin must be enabled and masked, and
 * an ISR routine provided. Since PCINTs are per port, not per pin, the ISR
 * must use some logic to actually implement a per-pin interrupt service.
 *
 *
 * Therefore the Arduino Pin to Interrupt map becomes:
 * Pin 0 = PCINT 8, Pin 10 = PCINT 4, Pin 11 = PCINT 5
 * Pin 12 = PCINT 6, Pin 13 = PCINT 17, Pin 14 = PCINT 10
 * Pin 15 = PCINT 9, Pin 53 = PCINT 0, Pin 52 = PCINT 1
 * Pin 51 = PCINT 2, Pin 50 = PCINT 3
 * Being we interested in the (50), (51), (52) and (53)
 * we will have to look at the PCINT 0/1/2/3 <-> PinArduino 53/52/51/50  -->B port
 *---------------------------------------------------------------------------*/

#define ASPIN 50  //4°bit del registro  0x08
#define ADPIN 51  //3°bit del registro  0x04  
#define PSPIN 52  //2°bit del registro  0x02
#define PDPIN 53  //1°bit del registro  0x01

typedef struct DatiEncoder{
  uint16_t pulseWidth;
  uint16_t edgeTime;
  byte counter;
  byte velocity;
}; 


volatile static uint8_t PCintLast;
volatile static DatiEncoder pinData[4];
char c[70]={'\0'};

void setup() {
  Serial.begin(BAUD);
  configureReceiver(); // Setup receiver pins for pin change interrupts
  Serial1.flush();
}

void loop() {
  //Invio dati!
  if (Serial.read()=='a'){
    cli();
    sprintf(c,"%05u\t%05u\t%05u\t%05u",pinData[0].pulseWidth,pinData[1].pulseWidth,pinData[2].pulseWidth,pinData[3].pulseWidth);
    Serial.print(c);
    Serial.print('\n');
    pinData[0].counter=0;
    pinData[1].counter=0;
    pinData[2].counter=0;
    pinData[3].counter=0;
    sei();
  }
}

void configureReceiver() {
  cli();
  PCMSK0 = 0;
  pinMode(ASPIN, INPUT_PULLUP);
  pinMode(ADPIN, INPUT_PULLUP);
  pinMode(PSPIN, INPUT_PULLUP);
  pinMode(PDPIN, INPUT_PULLUP);
  PCMSK0=0b00001111;
  PCICR = 0b00000001; // PCINT enabled only for the B port.
  sei();
}

ISR(PCINT0_vect,ISR_HOW) {
  uint8_t mask;
  uint8_t cPINB; // To read the Port B Input value
  uint16_t cTime; // To keep track of the current executing time, i.e. current Time
  cPINB = PINB;  //                                     // Let's save the Port B Input value as soon as possible  // modded 14/05
  cTime = micros();                                     // Reads the number of microseconds elapsed since the start of the code, with a resolution of 4 us
  mask = cPINB ^ PCintLast;                             // XORing the Port D Input with the last value saved we get mask for the changed bits
  PCintLast = cPINB;    // we save the Port B Input value for the next interrupt occurence
  if (mask & 0x08)
  {// Let's check the mask for the pin ASPIN
    pinData[0].counter++;
    if (!(cPINB & 0x08)) // True if the ASPIN is LOW
      pinData[0].pulseWidth = cTime - pinData[0].edgeTime; // Save the pulse width
    else // If instead the ASPIN is HIGH, let's keep track of the rise time
    pinData[0].edgeTime = cTime; // Updates the edgeTime for the pin with the cTime value
  }
  if (mask & 0x04)
  { // Let's check the mask for the pin ADPIN
    pinData[1].counter++;
    if (!(cPINB & 0x04)) // True if the ADPIN is LOW
      pinData[1].pulseWidth = cTime - pinData[1].edgeTime; // Save the pulse width
    else // If instead the ADPIN is HIGH, let's keep track of the rise time
    pinData[1].edgeTime = cTime; // Updates the edgeTime for the pin with the cTime value
  }
  if (mask & 0x02)
  { // Let's check the mask for the pin PSPIN
    pinData[2].counter++;
    if (!(cPINB & 0x02)) // True if the PSPIN is LOW
      pinData[2].pulseWidth = cTime - pinData[2].edgeTime; // Save the pulse width
    else // If instead the PSPIN is HIGH, let's keep track of the rise time
    pinData[2].edgeTime = cTime; // Updates the edgeTime for the pin with the cTime value
  }
  if (mask & 0x01)// << PDPIN) { // Let's check the mask for the pin PDPIN
  {
    pinData[3].counter++;
    if (!(cPINB & 0x01))// << PDPIN)) // True if the PDPIN is LOW
      pinData[3].pulseWidth = cTime - pinData[3].edgeTime; // Save the pulse width
    else // If instead the PDPIN is HIGH, let's keep track of the rise time
    pinData[3].edgeTime = cTime; // Updates the edgeTime for the pin with the cTime value
  }  
}
