// STERZO - VALORI DI DUTY CYCLE (ms, periodo in cui sta alta l'onda quadra)
#define DUTY_STERZO_DX 1064
#define DUTY_STERZO_NEUTRO 1476
#define DUTY_STERZO_SX 1890
// Proporzione dei valori precedenti in 16 bit
// Valore nominale = 6881
// Valori limite ottenuti = +/- 20% valore nominale
#define DUTY_SERVO_DX 5505   //4915
#define DUTY_SERVO_CENTRO 6881
#define DUTY_SERVO_SX 8257    //8847

// MOTORE - VALORI DI DUTY CYCLE (ms, periodo in cui sta alta l'onda quadra)
#define DUTY_MOTORE_MAX 2032
#define DUTY_MOTORE_NEUTRO 1496
#define DUTY_MOTORE_MIN 1000
// Proporzione dei valori precedenti in 16 bit
// Valore nominale = 7012
// Valori limite ottenuti = +/- 15% valore nominale
#define DUTY_ESC_MAX 8064
#define DUTY_ESC_NEUTRO 7010
#define DUTY_ESC_MIN 5960

// MODALITA - VALORI DI DUTY CYCLE (ms, periodo in cui sta alta l'onda quadra)
#define DUTY_MODALITA_ALTO 2032
#define DUTY_MODALITA_CENTRO 1496
#define DUTY_MODALITA_BASSO 1000

int val = 0;
int valS = 0;

void SetMotori(int pin_ESC, int pin_Servo) {
  SetPinFrequency(ESC, 71.4);
  SetPinFrequency(SERVO, 71.4);
}
void UpdateMotori() {
  valS = map(pinData[0].pulseWidth, DUTY_STERZO_DX, DUTY_STERZO_SX, DUTY_SERVO_DX, DUTY_SERVO_SX);
  if (pinData[1].pulseWidth <= DUTY_MOTORE_NEUTRO - 50) {
    pwmWriteHR(ESC, DUTY_ESC_NEUTRO);
  }
  if (pinData[1].pulseWidth >= DUTY_MOTORE_NEUTRO + 50) {
    val = map(pinData[1].pulseWidth, DUTY_MOTORE_MIN, DUTY_MOTORE_MAX, DUTY_ESC_NEUTRO, DUTY_ESC_MIN);
    pwmWriteHR(ESC, val);
  }
  if ((pinData[1].pulseWidth <= DUTY_MOTORE_NEUTRO + 50) && (pinData[1].pulseWidth >= DUTY_MOTORE_NEUTRO - 50))
    pwmWriteHR(ESC, DUTY_ESC_NEUTRO);
  pwmWriteHR(SERVO, valS);
}
void Sicurezza() {
    pwmWriteHR(ESC, DUTY_ESC_NEUTRO);
    pwmWriteHR(SERVO, DUTY_SERVO_CENTRO);
}
