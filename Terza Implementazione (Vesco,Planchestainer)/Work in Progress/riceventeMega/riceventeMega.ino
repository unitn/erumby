#include <PWM.h>
#include "pins_arduino.h"
#include "Arduino.h"
#include "inttypes.h"

#define ISR_HOW ISR_NOBLOCK //ISR_BLOCK  ISR_NOBLOCK

#define STERZO 51  //3°bit del registro  0x04  
#define MOTORE 52  //2°bit del registro  0x02
#define MODALITA 53  //1°bit del registro  0x01

#define ESC 11
#define SERVO 12

#define DUTY_MODALITA_ALTO 2024
#define DUTY_MODALITA_CENTRO 1504
#define DUTY_MODALITA_BASSO 980

typedef struct DatiRicevente {
  uint32_t pulseWidth;
  uint32_t edgeTime;
  uint16_t duty;
  byte counter;
};

volatile static uint8_t PCintLast;
volatile static DatiRicevente pinData[3];
char c[100] = {'\0'};

void setup() {
  InitTimersSafe();
  Serial.begin(57600);
  configureReceiver(); // Setup receiver pins for pin change interrupts
  Serial.flush();
  SetMotori(ESC,SERVO);
}

void loop() {
  //Invio dati!
  if (Serial.read() == 'a') {
    cli();
    PrintData();
    sei();
  }
  if ((pinData[2].pulseWidth>=DUTY_MODALITA_CENTRO-50) && (pinData[2].pulseWidth<=DUTY_MODALITA_CENTRO+50))
    UpdateMotori();
  if ((pinData[2].pulseWidth>=DUTY_MODALITA_BASSO-50) && (pinData[2].pulseWidth<=DUTY_MODALITA_BASSO+50))
    Sicurezza();
}

void PrintData() {
  Serial.print(pinData[0].pulseWidth);
  Serial.print('\t');
  Serial.print(pinData[1].pulseWidth);
  Serial.print('\t');
  Serial.print(pinData[2].pulseWidth);
  Serial.print('\n');
  pinData[0].counter = 0;
  pinData[1].counter = 0;
  pinData[2].counter = 0;

}

void configureReceiver() {
  cli();
  PCMSK0 = 0;
  pinMode(STERZO, INPUT_PULLUP);
  pinMode(MOTORE, INPUT_PULLUP);
  pinMode(MODALITA, INPUT_PULLUP);
  PCMSK0 = 0b00000111;
  PCICR = 0b00000001; // PCINT enabled only for the B port.
  sei();
}


ISR(PCINT0_vect, ISR_HOW) {
  uint8_t mask;
  uint8_t cPINB; // To read the Port B Input value
  uint32_t cTime; // To keep track of the current executing time, i.e. current Time
  cPINB = PINB;  //                                     // Let's save the Port B Input value as soon as possible  // modded 14/05
  cTime = micros();                                     // Reads the number of microseconds elapsed since the start of the code, with a resolution of 4 us
  mask = cPINB ^ PCintLast;                             // XORing the Port D Input with the last value saved we get mask for the changed bits
  PCintLast = cPINB;    // we save the Port B Input value for the next interrupt occurence
  if (mask & 0x04)
  { // Let's check the mask for the pin STERZO
    pinData[0].counter++;
    if (!(cPINB & 0x04)) // True if the STERZO is LOW
      pinData[0].pulseWidth = cTime - pinData[0].edgeTime; // Save the pulse width
    else // If instead the STERZO is HIGH, let's keep track of the rise time
      pinData[0].edgeTime = cTime; // Updates the edgeTime for the pin with the cTime value
  }
  if (mask & 0x02)
  { // Let's check the mask for the pin MOTORE
    pinData[1].counter++;
    if (!(cPINB & 0x02)) // True if the MOTORE is LOW
      pinData[1].pulseWidth = cTime - pinData[1].edgeTime; // Save the pulse width
    else // If instead the MOTORE is HIGH, let's keep track of the rise time
      pinData[1].edgeTime = cTime; // Updates the edgeTime for the pin with the cTime value
  }
  if (mask & 0x01) // Let's check the mask for the pin MODALITA
  {
    pinData[2].counter++;
    if (!(cPINB & 0x01))  // True if the MODALITA is LOW
      pinData[2].pulseWidth = cTime - pinData[2].edgeTime; // Save the pulse width
    else // If instead the MODALITA is HIGH, let's keep track of the rise time
      pinData[2].edgeTime = cTime; // Updates the edgeTime for the pin with the cTime value
  }
}
