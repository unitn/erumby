
uint32_t inizioTime; // tiene traccia del tempo corrente


void AutomaticoCamera() {
  //leggo la durata del comando per il servo e vado a mappare il valore letto tra i possibili per essere mandati in uscita.
  comandoSERVO = map(((int)(*enne)), -47, +47, DUTY_SERVO_DX, DUTY_SERVO_SX);
  comandoESC = 6750;
  pwmWriteHR(SERVO, comandoSERVO);
  pwmWriteHR(ESC, comandoESC);
}

void AutomaticoVelocita() {
  inizioTime = millis();
  /*if (inizioTime - startTime < 1000) {
    comandoESC = DUTY_ESC_NEUTRO;
    comandoSERVO = DUTY_SERVO_CENTRO;
    pwmWriteHR(SERVO, comandoSERVO);
    pwmWriteHR(ESC, comandoESC);
  }
  if ((inizioTime - startTime >= 1000) && (inizioTime - startTime <= 14000)) {

    comandoESC = DUTY_ESC_NEUTRO - (((inizioTime - startTime - 1000) / 125) * (DUTY_ESC_NEUTRO - DUTY_ESC_MIN) / 80);
    if (comandoESC < DUTY_ESC_MIN) {
      comandoESC = DUTY_ESC_MIN;
    }
    comandoSERVO = DUTY_SERVO_CENTRO;
    pwmWriteHR(SERVO, comandoSERVO);
    pwmWriteHR(ESC, comandoESC);
  }
  if (inizioTime - startTime > 14000) {
    comandoESC = DUTY_ESC_NEUTRO;    //variabile per inviare i comandi all'ESC
    comandoSERVO = DUTY_SERVO_CENTRO;
    pwmWriteHR(SERVO, comandoSERVO);
    pwmWriteHR(ESC, comandoESC);
  }*/
  if (inizioTime - startTime < 1000) {
    comandoESC = DUTY_ESC_NEUTRO;
    comandoSERVO = DUTY_SERVO_CENTRO;
    pwmWriteHR(SERVO, comandoSERVO);
    pwmWriteHR(ESC, comandoESC);
  }
  if ((inizioTime - startTime >= 1000) && (inizioTime - startTime <= 14000)) {
    comandoESC = atoi(controllo);
    comandoSERVO = DUTY_SERVO_CENTRO;
    pwmWriteHR(SERVO, comandoSERVO);
    pwmWriteHR(ESC, comandoESC);
  }
  if (inizioTime - startTime > 14000) {
    comandoESC = DUTY_ESC_NEUTRO;    //variabile per inviare i comandi all'ESC
    comandoSERVO = DUTY_SERVO_CENTRO;
    pwmWriteHR(SERVO, comandoSERVO);
    pwmWriteHR(ESC, comandoESC);
  }


}
