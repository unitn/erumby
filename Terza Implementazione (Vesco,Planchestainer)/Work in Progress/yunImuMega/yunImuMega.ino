//Receiver Code
#include "inttypes.h"
#define UPDATEENCODER 9000
#define UPDATEIMU 10000
#define SENDDATA 11000

unsigned long currentTime = 0;
unsigned long calculateEncoder = 0;
unsigned long calculateIMU = 0;
unsigned long calculateSend = 0;

char start = 'z';
char start1 = 'z';
char c[10] = {'\0'};


void setup() {
  Serial1.begin(19200);
  Serial.begin(19200);
  // Init sensors
  delay(50);  // Give sensors enough time to start
  I2C_Init();
  Accel_Init();
  Magn_Init();
  Gyro_Init();
  // Read sensors, init DCM algorithm
  delay(20);  // Give sensors enough time to collect data
  reset_sensor_fusion();
}

void loop() {
  currentTime = micros();
  start = Serial1.read();
  if (start == 'a') {
    start1 = start;
    start = 'z';
  }
  if ((currentTime >= (calculateEncoder + UPDATEENCODER)) && (start1 == 'a')) {
    EncoderUpdate();
    calculateEncoder = currentTime;
  }
  if ((currentTime >= (calculateIMU + UPDATEIMU)) && (start1 == 'a')) {
    IMUupdate();
    calculateIMU = currentTime;
  }
  if ((currentTime >= (calculateSend + SENDDATA)) && (start1 == 'a')) {
    sprintf(c, "%08u", currentTime - calculateSend);
    Serial1.print(c);
    PrintDataEncoder();
    PrintDataIMU();
    calculateSend = currentTime;
  }
}


