//Receiver Code
#include "SoftwareSerial.h"
#include "inttypes.h"
#define CALCULATEVELOCITY 40000
#define UPDATEIMU 50000


String str2;
unsigned long currentTime = 0;
unsigned long calculateVelocityTime = 0;
unsigned long calculateIMU = 0;
char start = 'z';
char start1 = 'z';
char c[100] = {'\0'};
char d[100] = {'\0'};

void PrintData();
void IMUupdate();

void setup() {
  Serial1.begin(19200);
  Serial.begin(19200);
  Serial1.flush();
  // Init sensors
  delay(50);  // Give sensors enough time to start
  I2C_Init();
  Accel_Init();
  Magn_Init();
  Gyro_Init();
  // Read sensors, init DCM algorithm
  delay(20);  // Give sensors enough time to collect data
  reset_sensor_fusion();
}

void loop() {
  currentTime = micros();
  start = Serial1.read();
  if (start == 'a') {
    start1 = start;
    start = 'z';
  }
  if ((currentTime >= (calculateVelocityTime + CALCULATEVELOCITY)) && (start1 == 'a')) {
    PrintData();
    calculateVelocityTime = currentTime;
  }
  if ((currentTime >= (calculateIMU + UPDATEIMU)) && (start1 == 'a')) {
    IMUupdate();
    calculateIMU = currentTime;
  }
}


