import sys    
import os
import serial
import time

s=serial.Serial("/dev/ttyATH0",57600,timeout=2)

s.flushInput()
s.flushOutput()

nome_prova='test1.txt'

file_txt=open(nome_prova,"w")

data_ora = time.asctime( time.localtime(time.time()) )

#intestazione_documento

file_txt.write("#PROVA :  " )
file_txt.write(nome_prova)
file_txt.write("\n#")
file_txt.write(data_ora)
file_txt.write("#[ms]\t[ms]\t[ms]\t[ms]\t[ms]\t[ms]\t[ms]\t[ms]\t[deg]\t[deg]\t[deg]\t[?]\t[?]\t[?]\t[?]\t[?]\t[?]\n")
file_txt.write("\n#DeltaT\todoFrontSx\todoFrontDx\todoRearSx\todoRearDx\tdutySterzo\tdutyAcceleratore\tdutyModalita\troll\tpitch\tyaw\tacc_x\tacc_y\tacc_z\tgyro_x\tgyro_y\tgyro_z\n")


a='a'
#invio valori a parte ATmega
s.write(a)
while True:
	if s.inWaiting()>0: # controlla se sono disponibili i dati nel buffer		
		print("//////  entrato  /////")
      		#lettura dei valori valori inviati dalla parte ATmega
      		tempo_yun=int(s.read(4))
		ant_sin= int(s.read(2))
     		ant_des= int(s.read(2))
		post_sin= int(s.read(2))
		post_des= int(s.read(2))
		duty_Sterzo= int(s.read(2))
		duty_Acceleratore= int(s.read(2))
		duty_Modalita =int(s.read(2))
		roll = float(s.read(4))
		pitch = float(s.read(4))
		yaw = float(s.read(4))
		acc_x= float(s.read(4))
		acc_y= float(s.read(4))
		acc_z=float(s.read(4))
		gyro_x=float(s.read(4))
		gyro_y=float(s.read(4))
		gyro_z=float(s.read(4))


           	#stampa dati sul terminale 
		print(tempo_yun)
		#scrittura valori nel file di testo
		file_txt.write(str(tempo_yun))
		file_txt.write("\t")
        	file_txt.write(str(ant_sin))
		file_txt.write("\t")
        	file_txt.write(str(ant_des))
 		file_txt.write("\t")
 		file_txt.write(str(post_sin))
 		file_txt.write("\t")
     	  	file_txt.write(str(post_des))
		file_txt.write("\t")				
     	  	file_txt.write(str(duty_Sterzo))
		file_txt.write("\t")
		file_txt.write(str(duty_Acceleratore))
		file_txt.write("\t")
		     	  	file_txt.write(str(duty_Modalita))
		file_txt.write("\t")
		     	  	file_txt.write(str(roll))
		file_txt.write("\t")
		     	  	file_txt.write(str(pitch))
		file_txt.write("\t")
		     	  	file_txt.write(str(yaw))
		file_txt.write("\t")
		     	  	file_txt.write(str(acc_x))
		file_txt.write("\t")
		     	  	file_txt.write(str(acc_y))
		file_txt.write("\t")
		     	  	file_txt.write(str(acc_z))
		file_txt.write("\t")
		     	  	file_txt.write(str(gyro_x))
		file_txt.write("\t")
		     	  	file_txt.write(str(gyro_y))
		file_txt.write("\t")
		     	  	file_txt.write(str(gyro_z))
		file_txt.write("\n")
file_txt.close()	
s.close()
