#include <stdio.h>
#include <math.h>

#define PIXEL_CNT 128        //numero di pixel che leggo dalla camera
#define C 5                  //numero del foro al quale è fissata la camera, 1 basso, 5 alto
#define BETA 30               //angolo di inclinazione della camera (gradi)
#define K 5                  //guadagno del controllore delle ruote
#define A_2 165                  //distanza tra il cdm della macchina e l'assale posteriore
#define L 330                   //distanza tra i due assali della macchina

#define a0 0            // TSL1401's analog output
#define si 7            // TSL1401's SI pin.
#define clk 8           // TSL1401's CLK pin.


#define EXPOSURE 5000//8333 
//Definizione costanti

//caratteristiche geometriche, dipendono dal montaggio
double c = 0;             //altezza finale
double b = 0;       //distanza alla quale vedo il pavimento
char start = 0;
// Variabili di output
int pdata[PIXEL_CNT];     // array che contine i dati in ingresso dal sensore
int background[PIXEL_CNT];  //array che contine il background
int res[PIXEL_CNT];        //array con la differenza tra dati in ingresso e background

//Variabili per i conti
typedef struct Minimo {    //struttura che contiene il punto di minimo in valore e posizione
  int valore;
  int posizione;
};

typedef struct Minimi {   //Struttura che contiene tutti i punti nell'intorno del minimo
  int valore[PIXEL_CNT];
  int posizione[PIXEL_CNT];
  int n;
};
Minimo punto;            //punto di minimo trovato
Minimo puntobg;            //punto di minimo con la Background Subtraction
Minimi puntibg;            //intorno del minimo con la Background Subtraction


// Inizializzazione
void setup() {
  Serial.begin(19200);         //comunicazione seriale con lo Yun
  Serial1.begin(19200);
  pinMode(si, OUTPUT);        //pin di uscita per si
  pinMode(clk, OUTPUT);       //pin di uscita per clk
  pinMode(13, OUTPUT);        //led della board
  switch (C) {                //in base al foro in cui è montata la camera ne definisco l'altezza finale
    case 1:
      c = 127.27 + 25.83 * cos(BETA);
      break;
    case 2:
      c = 144.27 + 25.83 * cos(BETA);
      break;
    case 3:
      c = 161.27 + 25.83 * cos(BETA);
      break;
    case 4:
      c = 178.27 + 25.83 * cos(BETA);
      break;
    case 5:
      c = 195.27 + 25.83 * cos(BETA);
      break;
  }
  b = c * tan(BETA);      //distanza in avanti alla quale la camera acquisisce campioni
  //clear ADCSRA and ADCSRB registers
  ADCSRA = 0;
  ADCSRB = 0;

  ADMUX |= (1 << REFS0); //set reference voltage
  ADMUX |= (1 << ADLAR); //left align the ADC value- so we can read highest 8 bits from ADCH register only

  ADCSRA |= (1 << ADPS2) | (1 << ADPS0); //set ADC clock with 32 prescaler- 16mHz/32=500kHz
  ADCSRA |= (1 << ADATE); //enabble auto trigger
  ADCSRA |= (1 << ADEN); //enable ADC
  ADCSRA |= (1 << ADSC); //start ADC measurements

  punto.valore = 0;
  punto.posizione = 64;

}

void loop() {
  puntibg.n = 0;               //svuoto gli intorni dei minimi
  start = Serial.read();       //lettura dei comandi da seriale
  if (start == 'a')                //'a' = invio dei dati
  {
    dispMin();               //Scrive sulla seriale solo il minimo trovato con i due algoritmi
  }
  if (start == 'g')
  {
    getBG(EXPOSURE);
  }
  if (start == 's') {
    getPix(EXPOSURE);            //funzione per l'acquisizione
    FindMinimiBg();          //algoritmo di Background Subtraction per trovare il minimo e il suo intorno
  }
}








