
//IPOTESI  c'è una sola linea, in caso contrario non va!!!
void FindMinimiBg()
{
  //cerco il minimo
  puntobg.valore = res[0];
  puntobg.posizione = 0;
  for (int i = 10; i < PIXEL_CNT - 10; i++) {
    if (puntobg.valore > res[i]) {
      puntobg.valore = res[i];
      puntobg.posizione = i;
    }
  }
  //cerco l'intorno
  if (puntobg.posizione != 0) {
    int media = 0;
    int posmedia = 0;
    for (int i = 10; i < PIXEL_CNT - 10; i++) {
      if (((res[i]) > (puntobg.valore - 4)) && (res[i] < (puntobg.valore + 4))) {
        puntibg.valore[puntibg.n] = res[i];
        puntibg.posizione[puntibg.n] = i;
        puntibg.n++;
        media += res[i];
        posmedia += i;
      }
    }
    if ((posmedia / puntibg.n >= 10) && (posmedia / puntibg.n <= 117)) {
      punto.valore = media / puntibg.n;
      punto.posizione = posmedia / (puntibg.n);
    }
  }
}

void clockSI(int dataPin, int clockPin) {
  digitalWrite(dataPin, HIGH);
  digitalWrite(clockPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(dataPin, LOW);
  digitalWrite(clockPin, LOW);
}
void getPix(int expose)
{
  //Mando un CLK e un SI per iniziare la letture.
  clockSI(si, clk);
  //Ripulisco i dati "vecchi".
  for (int i = 0; i < PIXEL_CNT; i++) {
    digitalWrite(clk, HIGH);
    digitalWrite(clk, LOW);
  }
  //Ritardo per lasciare il tempo di esposizione
  delayMicroseconds(expose);
  //Mando un altro CLK e SI per iniziare la lettura effettiva
  clockSI(si, clk);
  //Faccio la lettura dei dati reali inviando un CLK e leggendo il relativo dato
  for (int i = 0; i < PIXEL_CNT; i++) {
    digitalWrite(clk, HIGH);
    res[i] = ADCH - background[i]; //uso solo 256 livelli al posto di 1024 (filtro giÃ  un po' di disturbi) e sottraggo lo sfondo
    digitalWrite(clk, LOW);
  }
}

void getBG(int expose)
{
  //Mando un CLK e un SI per iniziare la letture.
  clockSI(si, clk);
  //Ripulisco i dati "vecchi".
  for (int i = 0; i < PIXEL_CNT; i++) {
    digitalWrite(clk, HIGH);
    digitalWrite(clk, LOW);
  }
  //Ritardo per lasciare il tempo di esposizione
  delayMicroseconds(expose);
  //Mando un altro CLK e SI per iniziare la lettura effettiva
  clockSI(si, clk);
  //Faccio la lettura dei dati reali inviando un CLK e leggendo il relativo dato
  for (int i = 0; i < PIXEL_CNT; i++) {
    digitalWrite(clk, HIGH);
    background[i] = ADCH;  //uso solo 256 livelli al posto di 1024
    digitalWrite(clk, LOW);
  }
}
void dispMin() {
  float diff = ((float)(punto.posizione - 64)) * (c / 128.0);   //distanza tra i due centri in mm
  byte* buff = (byte*)&diff;
  Serial.write(buff, 4);
  Serial1.write(buff, 4);
}


