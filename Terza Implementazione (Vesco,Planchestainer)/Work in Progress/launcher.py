import sys    
import os
import serial
import time

s=serial.Serial("/dev/ttyATH0",19200,timeout=2)

s.flushInput()
s.flushOutput()
nome_prova='test1.txt'

file_txt=open(nome_prova,"w")

data_ora = time.asctime( time.localtime(time.time()) )

#intestazione_documento

file_txt.write("#PROVA :  " )
file_txt.write(nome_prova)
file_txt.write("\n#")
file_txt.write(data_ora)
file_txt.write("\n#")

file_txt.write("[ms]\t[ms]\t[ms]\t[ms]\t[ms]\n" )
file_txt.write("#DeltaT\tAS\tAD\tPS\tPD\n")

a='a'

#invio valori a parte ATmega
s.write(a)
while True:
	if s.inWaiting()>0: # controlla se sono disponibili i dati nel buffer		
		print("//////  entrato  /////")

      		#lettura dei valori valori inviati dalla parte ATmega
      		tempo_yun_long=s.read(8)
		ant_sin= s.read(5)
     		ant_des= s.read(5)
		post_sin= s.read(5)
		post_des= s.read(5)

           	#stampa dati sul terminale 
		print(tempo_yun_long)
      		print(ant_sin,ant_des,post_sin,post_des)


		#scrittura valori nel file di testo
		file_txt.write(tempo_yun_long)
		file_txt.write("\t")
        	file_txt.write(ant_sin)
		file_txt.write("\t")
        	file_txt.write(ant_des)
 		file_txt.write("\t")
 		file_txt.write(post_sin)
 		file_txt.write("\t")
     	  	file_txt.write(post_des)
		file_txt.write("\n")
file_txt.close()	
s.close()
