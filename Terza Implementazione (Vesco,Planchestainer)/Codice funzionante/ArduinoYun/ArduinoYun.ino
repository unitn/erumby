#include <SoftwareSerial.h>
#include "inttypes.h"
#define SENDDATA 17000   // 58.82 Hz


//variabili per tenere conto del tempo
unsigned long currentTime = 0;  //tempo che invia al LININO
unsigned long t0;               //tempo iniziale del ciclo di loop
unsigned long t1;               //tempo impiegato per aggiornare la IMU
unsigned long t2;               //tempo impiegato per leggere gli encoder

char start = 'z';               //variabile che legge il momento d'inizio del ciclo di acquisizione
char start1 = 'z';              //variabile secondaria che mantiene il ciclo di invio dei dati attivo
byte* c;                        //buffer d'uscita
byte dataMega[30] = {0};        //buffer per i dati letti dal Mega di encoder/ricevente
byte dataCamera[4] = {0};      //buffer per i dati letti dal Mega della camera

SoftwareSerial fromMega(10, 11); //RX,TX   seriale con il Mega di encoder/ricevente
SoftwareSerial fromCamera(8, 9); //RX,TX    seriale con il mega della camera

void setup() {
  Serial1.begin(115200);          //inizio comunicazione con il LININO
  fromMega.begin(19200);          //inizio comunicazione con il Mega di encoder/ricevente
  fromCamera.begin(19200);      //inizio comunicazione con il Mega della camera

  //Inizializzazione della IMU
  delay(50);  // Give sensors enough time to start
  I2C_Init();
  Accel_Init();
  Magn_Init();
  Gyro_Init();
  // Read sensors, init DCM algorithm
  delay(20);  // Give sensors enough time to collect data
  reset_sensor_fusion();
}

void loop() {
  t0 = micros();   //tempo iniziale del loop
  start = Serial1.read();  //controllo se è il momento di iniziare a inviare
  if (start == 'a') {
    start1 = start;        //salvo il valore per inviare continuamente i dati
    start = 'z';
  }
  if (start1 == 'a') {
    fromCamera.listen();
    fromCamera.write('s');
  }
  if (start == 'g') {             //utilizzando il corretto script in Python, invio qui una 'g',
    fromCamera.listen();
    fromCamera.write('g');
    fromMega.listen();
    fromMega.write('g');
    byte buff[2];
    Serial1.readBytes(buff, 2);
    fromMega.write(buff, 2);
  }
  IMUupdate();              //aggiornamento della IMU
  t1 = micros() - t0;       //~quanto tempo ho impiegato ad aggiornare la IMU
  if (start1 == 'a')
  { //se è iniziato il ciclo di invio dati
    fromMega.listen();
    EncoderUpdate();        //comunicazione seriale per ricevere i dati di encoder/ricevente
    fromCamera.listen();
    CameraUpdate();       //comunicazione seriale per ricevere i dati della camera e inviare quelli richiesti
    t2 = micros() - t1 - t0;    //quanto tempo ho impiegato
    delayMicroseconds(SENDDATA - t1 - t2); //aspetto finchè non arrivo ad avere SENDDATA usec tra un invio e l'altro
    currentTime = micros() - t0;          //tempo che ci ho impiegato ad arrivare qui
    c = (byte*)&currentTime;              //salvo nel buffer d'uscita
    Serial1.write(c, 4);                  //scrittura dati tempo
    PrintDataEncoder();                   //scrittura dati encoder+ricevente
    PrintDataIMU();                       //scrittura dati IMU
    PrintDataCamera();                  //scrittura dati camera
  }
}