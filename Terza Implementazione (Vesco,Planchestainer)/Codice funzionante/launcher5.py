import sys    
import os
import serial
import time
from struct import *

s=serial.Serial("/dev/ttyATH0",115200,timeout=2)

s.flushInput()
s.flushOutput()

nome_prova='test1_0902.txt'

file_txt=open(nome_prova,"w")

data_ora = time.asctime( time.localtime(time.time()) )

#intestazione_documento

file_txt.write("#PROVA :  " )
file_txt.write(nome_prova)
file_txt.write("\n#")
file_txt.write(data_ora)
file_txt.write("\n#[ms]\t[rad/s]\t[rad/s]\t[rad/s]\t[rad/s]\t[rad/s]\t[ms]\t[ms]\t[ms]\t[int]\t[int]\t[deg]\t[deg]\t[deg]\t[g]\t[g]\t[g]\t[?]\t[?]\t[?]\t[mm]\n")
file_txt.write("\n#DeltaT\todoFrontSx\todoFrontDx\todoRearSx\todoRearDx\todoMedia\tdutySterzo\tdutyAcceleratore\tdutyModalita\tcomandoServo\tcomandoESC\troll\tpitch\tyaw\tacc_x\tacc_y\tacc_z\tgyro_x\tgyro_y\tgyro_z\t enne\n")

a='a'
#invio valori a parte ATmega
s.write(a)
while True:
	if s.inWaiting()>0: # controlla se sono disponibili i dati nel buffer		
		print("//////  entrato  /////")
      		#lettura dei valori valori inviati dalla parte ATmega
		#record ='b'
		record = s.read(74)
      		tempo_yun,ant_sin,ant_des,post_sin,post_des,media,duty_Sterzo,duty_Acceleratore,duty_Modalita,comandoServo,comandoESC,roll,pitch,yaw,acc_x,acc_y,acc_z,gyro_x,gyro_y,gyro_z,enne=unpack('<IfffffHHHHHffffffffff',record)

           	#stampa dati sul terminale 
		#scrittura valori nel file di testo
		file_txt.write(str(tempo_yun))
		file_txt.write("\t")
        	file_txt.write(str(ant_sin))
		file_txt.write("\t")
        	file_txt.write(str(ant_des))
 		file_txt.write("\t")
 		file_txt.write(str(post_sin))
 		file_txt.write("\t")
     	  	file_txt.write(str(post_des))
		file_txt.write("\t")
     	  	file_txt.write(str(media))
		file_txt.write("\t")					
     	  	file_txt.write(str(duty_Sterzo))
		file_txt.write("\t")
		file_txt.write(str(duty_Acceleratore))
		file_txt.write("\t")
		file_txt.write(str(duty_Modalita))
		file_txt.write("\t")
		file_txt.write(str(comandoSERVO))
		file_txt.write("\t")
		file_txt.write(str(comandoESC))
		file_txt.write("\t")
		file_txt.write(str(roll))
		file_txt.write("\t")
		file_txt.write(str(pitch))
		file_txt.write("\t")
		file_txt.write(str(yaw))
		file_txt.write("\t")
		file_txt.write(str(acc_x))
		file_txt.write("\t")
		file_txt.write(str(acc_y))
		file_txt.write("\t")
		file_txt.write(str(acc_z))
		file_txt.write("\t")
		file_txt.write(str(gyro_x))
		file_txt.write("\t")
		file_txt.write(str(gyro_y))
		file_txt.write("\t")
		file_txt.write(str(gyro_z))
		file_txt.write("\t")
		file_txt.write(str(enne))
		file_txt.write("\n")
file_txt.close()	
s.close()
